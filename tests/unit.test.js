const Joi = require("joi");
const archiotect_config = require("./../lib/archiotect_config");
const _ = require("lodash");
const Promise = require("bluebird");

describe("Archiotect_config", () =>
{
    test("Load without config", () =>
    {
        return archiotect_config.load(undefined,
            {})
            .then((res) =>
            {
                expect(_.isObject(res))
                    .toBe(true);
                expect(!_.isUndefined(res.validate))
                    .toBe(true);
                expect(_.isFunction(res.validate))
                    .toBe(true);
            })
    });

    test("Load with false config", () =>
    {
        expect(() =>
            {
                return archiotect_config.load(false,
                {})
            })
            .toThrow();
    });

    test.each(
        [[false, "pre_load"],
    [true, "pre_load"],
    [false, "post_load"],
    [true, "post_load"]])
        ("Validate a plugin : config=%s, exec_time=%s", (config, et) =>
        {
            var plugin = {
                name: "toto",
                version: "1.0.0",
                need: [],
                load: () => false,
                config_schema: config ? Joi.object()
                    .keys(
                    {
                        toto: Joi.string()
                            .required()
                    })
                    .required() : undefined,
                config:
                {
                    toto: "tutu"
                }
            };
            return archiotect_config.load(undefined,
                {})
                .then(() => archiotect_config.handle(et, plugin))
                .then((res) =>
                {
                    expect(res)
                        .toBe(false);
                    expect(plugin.config)
                        .toBeDefined();
                    expect(_.isObject(plugin.config))
                        .toBe(true);
                    expect(_.get(plugin, "config.toto"))
                        .toBe("tutu");
                });
        });

    test.each(
            [[false, "pre_load"],
        [true, "pre_load"],
        [false, "post_load"],
        [true, "post_load"]])
        ("Validate a plugin (should fail): config=%s, exec_time=%s", (config, et) =>
        {
            var plugin = {
                name: "toto",
                version: "1.0.0",
                need: [],
                load: () => false,
                config_schema: config ? Joi.object()
                    .keys(
                    {
                        toto: Joi.string()
                            .required()
                    })
                    .required() : undefined,
                config: false
            };

            return archiotect_config.load(undefined,
                {})
                .then(() => archiotect_config.handle(et, plugin))
                .then((res) =>
                {
                    if (et === "pre_load" && config)
                        return expect(res)
                            .toBeTruthy();
                    else
                        expect(res)
                        .toBe(false);
                    expect(plugin.config)
                        .toBeDefined();
                    expect(_.isObject(plugin.config))
                        .not
                        .toBe(true);
                });
        });

    test.each(["pre_load", "post_load"])
        ("Change exec time : %s", (et) =>
        {
            var plugin = {
                name: "toto",
                version: "1.0.0",
                need: [],
                load: () => false,
                config_schema: Joi.object()
                    .keys(
                    {
                        toto: Joi.string()
                            .required()
                    })
                    .required(),
                config: false
            };

            return archiotect_config.load(
                {
                    exec_time: et
                },
                {})
                .then(() =>
                {
                    return Promise.each(["pre_load", "post_load"], (et_now) =>
                    {
                        let tmp = _.cloneDeep(plugin);

                        return archiotect_config.handle(et_now, tmp)
                            .then((res) =>
                            {
                                if (et_now === et)
                                    return expect(res)
                                        .toBeTruthy();
                                else
                                    expect(res)
                                    .toBe(false);
                                if (!res)
                                    expect(plugin.config)
                                    .toBeDefined();
                                else
                                    expect(plugin.config)
                                    .not
                                    .toBeDefined();
                                expect(_.isObject(plugin.config))
                                    .not
                                    .toBe(true);
                            })
                    });
                });
        });

    test.each(["toto", "tutu"])
        ("Change schema var name : %s", (name) =>
        {
            var plugin = {
                name: "toto",
                version: "1.0.0",
                need: [],
                load: () => false,
                config:
                {
                    toto: "tutu"
                }
            };

            plugin[name] = Joi.object()
                .keys(
                {
                    toto: Joi.string()
                        .required()
                })
                .required();
            return archiotect_config.load(
                {
                    exec_time: "pre_load",
                    schema_var_name: name
                },
                {})
                .then(() => archiotect_config.handle("pre_load", plugin))
                .then((res) =>
                {
                    expect(res)
                        .toBe(false);
                    expect(plugin.config)
                        .toBeDefined();
                    expect(_.isObject(plugin.config))
                        .toBe(true);
                    expect(plugin.config.toto)
                        .toBe("tutu");
                });
        });

        test.each(["toto", "tutu"])
        ("Change result var name : %s", (name) =>
        {
            var plugin = {
                name: "toto",
                version: "1.0.0",
                need: [],
                load: () => false,
                config:
                {
                    toto: "tutu"
                }
            };

            plugin["config_schema"] = Joi.object()
                .keys(
                {
                    toto: Joi.string()
                        .required()
                })
                .required();
            return archiotect_config.load(
                {
                    exec_time: "pre_load",
                    result_var_name: name
                },
                {})
                .then(() => archiotect_config.handle("pre_load", plugin))
                .then((res) =>
                {
                    expect(res)
                        .toBe(false);
                    expect(plugin[name])
                        .toBeDefined();
                    expect(_.isObject(plugin[name]))
                        .toBe(true);
                    expect(plugin[name].toto)
                        .toBe("tutu");
                });
        });
})