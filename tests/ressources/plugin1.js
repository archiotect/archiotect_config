const Joi = require("joi");
module.exports = {
    name: "plugin1",
    version: "1.0.0",
    need: [],
    load: (config, plugins) =>
    {
        return {
            config_schema: Joi.object()
                .keys(
                {
                    toto: Joi.string()
                        .required()
                })
                .required()
        };
    }
}