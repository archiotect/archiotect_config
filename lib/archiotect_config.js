"use strict";

const Joi = require("joi");
const _ = require("lodash");
const Promise = require("bluebird");

const config_schema = Joi.object()
    .keys(
    {
        exec_time: Joi.string()
            .default("pre_load")
            .allow(["pre_load", "post_load"]),
        schema_var_name: Joi.string()
            .default("config_schema"),
        result_var_name: Joi.string()
            .default("config")
    })
    .default(
    {
        exec_time: "pre_load",
        schema_var_name: "config_schema",
        result_var_name: "config"
    });
var plugin_config;

function validate(conf, schema)
{
    var vali = Joi.validate(conf, schema);

    return new Promise((resolve, reject) =>
    {
        if (vali.error)
            return reject(vali.error);
        return resolve(vali.value);
    })
}

module.exports = {
    name: "Archiotect_config",
    version: "0.1.0",
    need: [],
    core: true,
    load: (conf) =>
    {
        var vali = Joi.validate(conf, config_schema);
        if (vali.error)
            throw vali.error;
        plugin_config = vali.value;

        return Promise.resolve(
        {
            validate
        });
    },
    handle: (exec_time, plugin) =>
    {
        if (exec_time === plugin_config.exec_time &&
            _.isObject(_.get(plugin, plugin_config.schema_var_name)) &&
            _.get(plugin, plugin_config.schema_var_name)
            .isJoi)
            return validate(plugin.config, _.get(plugin, plugin_config.schema_var_name))
                .then((value) =>
                {
                    _.set(plugin, plugin_config.result_var_name, value);
                    return false;
                })
                .catch((err) =>
                {
                    _.unset(plugin, plugin_config.result_var_name);
                    return err;
                });
        return Promise.resolve(false);
    }
}